'use strict'

module.exports = {
    userId: String,
    image: {
        url: String,
        thumbnail: String,
        resize_url: String,
        resize_thumbnail: String
    }
}