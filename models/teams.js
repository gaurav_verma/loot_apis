'use strict'

// images Module
module.exports = {
    category: {
        _id: String
    },
    name: String,
    description: String,
    image: {
        url: String,
        thumbnail: String,
        resize_url: String,
        resize_thumbnail: String,
    },
    teams: [{
        url: String,
        thumbnail: String,
        resize_url: String,
        resize_thumbnail: String,
    }]
}