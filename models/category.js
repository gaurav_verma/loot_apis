'use strict'

module.exports = {
    name: String,
    image: {
        url: String,
        thumbnail: String,
        resize_url: String,
        resize_thumbnail: String,
    },
    quoteImages: [{
        _id: String,
        name: String,
        image: {
            url: String,
            thumbnail: String,
            resize_url: String,
            resize_thumbnail: String,
        }

    }]
}