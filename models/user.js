'use strict'

// User Module
module.exports = {
    type: {
        type: String,
        default: 'user',
        enum: ['user', 'admin']
    },
    name: {
        type: String,
        lowercase: true,
        trim: true
    },
    email: {
        type: String,
        lowercase: true,
        trim: true
    },
    password: String,
    expiryTime: String,
    otp: String,
    isVerified: {
        type: Boolean,
        default: false
    },
    token: String,
    profile: {
        image: {
            url: String,
            thumbnail: String,
            resize_url: String,
            resize_thumbnail: String
        },
        status: String
    },
    chapters: [{
        chapterId: String
    }]

}


// tempToken: String,
// regToken:String,
