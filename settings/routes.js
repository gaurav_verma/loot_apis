'use strict'

const fs = require('fs')
const specs = require('../specs')
const api = require('../api')
var auth = require('../permit')
const validator = require('../validators')
const fileUpload = require('express-fileupload');
const bodyParser = require('body-parser');

var multipart = require('connect-multiparty')
var multipartMiddleware = multipart()


const configure = (app, logger) => {
    const log = logger.start('settings:routes:configure')

    app.get('/specs', function (req, res) {
        fs.readFile('./public/specs.html', function (err, data) {
            if (err) {
                return res.json({
                    isSuccess: false,
                    error: err.toString()
                })
            }
            res.contentType('text/html')
            res.send(data)
        })
    })

    app.get('/api/specs', function (req, res) {
        res.contentType('application/json')
        res.send(specs.get())
    })


    // .......................users routes..............................


    app.get('/api/users', auth.context.builder, auth.context.requiresToken, api.users.get);

    app.post('/api/users/verifyUser', auth.context.builder, validator.users.verifyUser, api.users.verifyUser);
    app.post('/api/users', auth.context.builder, validator.users.canCreate, api.users.create);

    app.get('/api/users/:id', auth.context.builder, auth.context.requiresToken, validator.users.getById, api.users.getById);
    app.put('/api/users/:id', auth.context.builder, auth.context.requiresToken, validator.users.update, api.users.update);
    app.post('/api/users/login', auth.context.builder, validator.users.login, api.users.login);

    app.post('/api/users/forgotPassword', auth.context.builder, validator.users.forgotPassword, api.users.forgotPassword);
    app.post('/api/users/resetPassword', auth.context.builder, validator.users.resetPassword, api.users.resetPassword);

    app.post('/api/users/changePassword', auth.context.builder, auth.context.requiresToken, validator.users.changePassword, api.users.changePassword);
    app.post('/api/users/logOut', auth.context.builder, auth.context.requiresToken, api.users.logOut)

    // ................................upload files............................................
    app.post("/api/files", auth.context.builder, multipartMiddleware, api.files.create);
    app.post("/api/files/upload", multipartMiddleware, api.files.upload);
    app.get("/api/files/:id", auth.context.builder, api.files.getById);

    // .................... category routes.................................
    app.post('/api/categories', auth.context.builder, api.categories.create);
    app.get('/api/categories/:id', auth.context.builder, api.categories.getById);
    app.get('/api/categories', auth.context.builder, api.categories.get);

    //...................similarProducts routes................................
    app.post('/api/teams', auth.context.builder, api.teams.create);
    app.get('/api/teams', auth.context.builder, api.teams.getByCategory);
    app.get('/api/teams/:id', auth.context.builder, api.teams.getById);
    app.delete('/api/teams/delete/:id', auth.context.builder, api.teams.deleteImage)

    log.end()
}
exports.configure = configure