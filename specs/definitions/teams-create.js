module.exports = {
    category: {
        _id: 'string'
    },
    name: 'string',
    description: 'string',
    image:{
        url: 'string',
        thumbnail: 'string',
        resize_url: 'string',
        resize_thumbnail: 'string'
    },
    teams: [{
        url: 'string',
        thumbnail: 'string',
        resize_url: 'string',
        resize_thumbnail: 'string',
    }]
}
