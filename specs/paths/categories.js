module.exports = [{
    url: '/',
    get: {
        summary: 'Search',
        description: 'get Category list',
        parameters: [],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    },
    post: {
        summary: 'Create category',
        description: 'Create category',
        parameters: [
        //     {
        //     in: 'headers',
        //     name: 'x-access-token',
        //     description: 'token to access api',
        //     required: true,
        //     type: 'string'
        // },
         {
            in: 'body',
            name: "body",
            description: 'Model of document creation',
            required: true,
            schema: {
                $ref: '#/definitions/categoryCreateReq'
            }
        }],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    }
}, {
    url: '/{id}',
    get: {
        summary: 'Get',
        description: 'get category by Id',
        parameters: [
            // {
            //     in: 'header',
            //     name: 'x-access-token',
            //     description: 'token to access api',
            //     required: true,
            //     type: 'string'
            // },
            {
                in: 'path',
                name: 'id',
                description: 'categoryId',
                required: true,
                type: 'string'
            }
        ],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    }
}]