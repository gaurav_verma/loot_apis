module.exports = [{
    url: '/',
    get: {
        summary: 'Search',
        description: 'get teams list',
        parameters: [{
            in: 'query',
            name: 'categoryId',
            description: 'categoryId',
            required: true,
            type: 'string'
        }],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    },
    post: {
        summary: 'Create image',
        description: 'Create image',
        parameters: [
         {
            in: 'body',
            name: "body",
            description: 'Model of image creation',
            required: true,
            schema: {
                $ref: '#/definitions/teamsCreateReq'
            }
        }],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    }
}, {
    url: '/{id}',
    get: {
        summary: 'Get',
        description: 'get image by Id',
        parameters: [
            {
                in: 'path',
                name: 'id',
                description: 'imageId',
                required: true,
                type: 'string'
            }
        ],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    }
},
{
    url: '/delete/{id}',
    delete: {
        summary: 'Get',
        description: 'get image by Id',
        parameters: [
            {
                in: 'path',
                name: 'id',
                description: 'imageId',
                required: true,
                type: 'string'
            }
        ],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    }
}
]