'use strict'
const imageUploadService = require('./files')
// const response = require('../exchange/response')


// model pass in response
const createTempProductObj = async (product, count, skipCount, totalCount) => {
    var productObj = {
        products: product,
        count: count,
        skipCount: skipCount,
        totalCount: totalCount
    }

    return productObj;
}

// set method to update similarProduct
const set = async (body, req, entity, context) => {
    try {
        let data
        if (req) {
            if (req.files != null && req.files != undefined) {
                if (req.files.file != null && req.files.file != undefined) {

                    if (body.shouldImageUpdate == "true") {

                        data = await imageUploadService.upload(req.files.file)

                        if (body.name) {
                            entity.name = body.name
                        }
                        if (body.brand) {
                            entity.brand = body.brand
                        }
                        if (data.url) {
                            entity.image.url = data.url
                        }
                        if (data.thumbnail) {
                            entity.image.thumbnail = data.thumbnail
                        }
                        if (data.resize_url) {
                            entity.image.resize_url = data.resize_url
                        }
                        if (data.resize_thumbnail) {
                            entity.image.resize_thumbnail = data.resize_thumbnail
                        }
                        if (body.heading && body.heading.title) {
                            entity.heading.title = body.heading.title
                        }
                        if (body.heading && body.heading.description) {
                            entity.heading.description = body.heading.description
                        }
                        if (!entity.dimensions[0]) {
                            if (body.dimensions && body.dimensions.value) {
                                entity.dimensions[0] = body.dimensions
                            }
                        } else {
                            if (body.dimensions && body.dimensions.value) {
                                entity.dimensions[0].value = body.dimensions.value
                            }
                            if (body.dimensions && body.dimensions.unit) {
                                entity.dimensions[0].unit = body.dimensions.unit
                            }
                        }
                        if (body.price && body.price.value) {
                            entity.price.value = body.price.value
                        }
                        if (body.price && body.price.currency) {
                            entity.price.currency = body.price.currency
                        }
                        if (body.status) {
                            entity.status = body.status
                        }
                        if (body.isAdded) {
                            entity.isAdded = body.isAdded
                        }
                        if (body.shouldImageUpdate) {
                            entity.shouldImageUpdate = body.shouldImageUpdate
                        }

                    } else {

                        if (body.name) {
                            entity.name = body.name
                        }
                        if (body.brand) {
                            entity.brand = body.brand
                        }
                        if (body.heading && body.heading.title) {
                            entity.heading.title = body.heading.title
                        }
                        if (body.heading && body.heading.description) {
                            entity.heading.description = body.heading.description
                        }
                        if (!entity.dimensions[0]) {
                            if (body.dimensions && body.dimensions.value) {
                                entity.dimensions[0] = body.dimensions
                            }
                        } else {
                            if (body.dimensions && body.dimensions.value) {
                                entity.dimensions[0].value = body.dimensions.value
                            }
                            if (body.dimensions && body.dimensions.unit) {
                                entity.dimensions[0].unit = body.dimensions.unit
                            }
                        }
                        if (body.price && body.price.value) {
                            entity.price.value = body.price.value
                        }
                        if (body.price && body.price.currency) {
                            entity.price.currency = body.price.currency
                        }
                        if (body.status) {
                            entity.status = body.status
                        }
                        if (body.isAdded) {
                            entity.isAdded = body.isAdded
                        }

                    }
                }
            }
        }

        return entity
    } catch (err) {
        throw new Error(err)
    }
}


const create = async (body, context) => {
    const log = context.logger.start(`services/teams`)
    try {
        let image
        image = await new db.teams(body).save()
        log.end()
        return image
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const getByCategory = async (req, context) => {
    const log = context.logger.start(`services/getByCategory`)
    try {
        const params = req.query;
        let images;

        if (params.categoryId) {
            images = await db.teams.find({
                'category._id': {
                    $eq: params.categoryId
                }
            }).sort({
                timeStamp: -1
            })
            log.end()
        } else {
            // find products
            images = await db.teams.find({}).sort({
                timeStamp: -1
            })
            log.end()
        }
        return images
    } catch (err) {
        throw new Error(err)
    }
}

const getById = async (id, context) => {
    const log = context.logger.start(`services/getByCategory`)
    try {
        const images = await db.teams.findById(id)
        log.end()
        return images
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const deleteImage = async (id, context,res) => {
    const log = context.logger.start(`services/deleteImage`)
    let image
         image = await db.teams.findById(id)
        log.end()
        if(image){
            await db.teams.deleteOne({ _id: id })
        }
        res.message='image deleted successfully'
        return res.message
    
}

exports.create = create
exports.getByCategory = getByCategory
exports.getById = getById
exports.deleteImage=deleteImage
