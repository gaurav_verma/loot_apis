'use strict'
const service = require('./teams')
// const imageUploadService = require('../services/files')
const response = require('../exchange/response')
// "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZGQ0MzEzNTE0NDQwNDE4MTRmYzFjMjUiLCJpYXQiOjE1NzQxODczNjl9.g_GYqWSGGLQ26FaT5DVDukdltP1OCNUKEEVwp3HtNeY"


//  model pass in response 
const createTempProductObj = async (products, count, skipCount, totalCount) => {
    var productObj = {
        categories: products,
        count: count,
        skipCount: skipCount,
        totalCount: totalCount
    }

    return productObj;
}

// set method to update category
const set = async (body, req, entity, context) => {
    try {
        let data
        if (req) {
            if (req.files != null && req.files != undefined) {
                if (req.files.file != null && req.files.file != undefined) {

                    if (body.shouldImageUpdate == "true") {

                        data = await imageUploadService.upload(req.files.file)

                        if (body.name) {
                            entity.name = body.name
                        }
                        if (data.url) {
                            entity.image.url = data.url
                        }
                        if (data.thumbnail) {
                            entity.image.thumbnail = data.thumbnail
                        }
                        if (data.resize_url) {
                            entity.image.resize_url = data.resize_url
                        }
                        if (data.resize_thumbnail) {
                            entity.image.resize_thumbnail = data.resize_thumbnail
                        }
                        if (body.status) {
                            entity.status = body.status
                        }
                        if (body.shouldImageUpdate) {
                            entity.shouldImageUpdate = body.shouldImageUpdate
                        }
                    } else {
                        if (body.name) {
                            entity.name = body.name
                        }
                        if (body.status) {
                            entity.status = body.status
                        }
                    }
                }
            }
        }

        return entity

    } catch (err) {
        throw new Error(err)
    }
}


const create = async (model, context) => {
    const log = context.logger.start(`services/categories`)
    try {
        let category
        category = await db.category.findOne({
            'name': {
                $eq: model.name
            }
        })
        if (!category) {
            category = await new db.category(model).save()
        } else {
            throw new Error('Category already exists')
        }

        log.end()
        return category

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const getById = async (id, context) => {
    const log = context.logger.start(`services/category/get:${id}`)
    try {
        var req = {
            query: {
                'categoryId': id
            }
        }
        const category = await db.category.findById(id)
        const images = await service.getByCategory(req,context)
        category.quoteImages = images;

        // category.count = ''
        // category.skipCount = ''
        // category.totalCount = ''
        log.end()
        return category
    } catch (err) {
        throw new Error(err)
    }
}

const get = async (req) => {
    try {
        const params = req.query;
        let category;
        
        category = await db.category.find({}).sort({
            timeStamp: -1
        })

        // let count = ''
        // let skippedCount = ''
        // let totalCount = ''

        // const tempProductResponseObj = await createTempProductObj(category, count, skippedCount, totalCount)
        // category = tempProductResponseObj

        return category

    } catch (err) {
        throw new Error(err)
    }
}


exports.create = create
exports.getById = getById
exports.get = get
