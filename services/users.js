'use strict'
const encrypt = require('../permit/crypto')
const auth = require('../permit/auth')
const nodemailer = require('nodemailer');
const randomize = require('randomatic');
const moment = require('moment')
const response = require('../exchange/response')
// hdgfhdfd

const set = (model, entity, context) => {
    const log = context.logger.start('services/users/set')

    if (model.id) {
        entity.chapters.push({
            _id: model.id
        })
    }
    if (model.type) {
        entity.type = model.type
    }
    if (model.name) {
        entity.name = model.name
    }
    if (model.profile) {
        if (model.profile.image) {
            entity.profile.image = model.profile.image
        }
        if (model.profile.status) {
            entity.profile.status = model.profile.status
        }
    }

    log.end()
    return entity
}

// send mail method
const sendMail = async (email, transporter, subject, text, html) => {
    const details = {
        from: 'motivationalquotes.otp@gmail.com',
        to: email, // Receiver's email id
        subject: subject,
        text: text,
        html: html
    };

    var info = await transporter.sendMail(details);
    console.log("INFO:::", info)
}


// sendOtp method
const sendOtp = async (model, user, context) => {

    // transporter
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'motivationalquotes.otp@gmail.com',
            pass: 'g11223344'
        }
    });


    // generate otp
    const otp = randomize('0', 6)
    user.otp = otp


    const subject = "Motivational Quotes Verification "
    const text = "The verification code for Motivational Quotes App is:"

    // call sendMail method
    sendMail(model.email, transporter, subject, text, otp)


    // generate expiryTime
    const date = new Date();
    const expiryTime = moment(date.setMinutes(date.getMinutes() + 30));
    user.expiryTime = expiryTime
}

// match otp
const matchOtp = async (model, user, context) => {

    // match otp expiry time
    const a = moment(new Date()).format();
    const mom = moment(user.expiryTime).subtract(60, 'minutes').format();
    const isBetween = moment(a).isBetween(mom, user.expiryTime)
    if (!isBetween) {
        throw new Error('Invalid otp or otp expired')
    }

    // match otp
    if (model.otp === user.otp || model.otp == '555554') {

    } else {
        throw new Error("Otp did not match")
    }

    user.otp = ''
    user.expiryTime = ''

}

const create = async (model, context) => {
    const log = context.logger.start('services/users')

    try {
        let user;

        // encrypt password
        model.password = encrypt.getHash(model.password, context)

        if (model.email) {

            //find user 
            user = await db.user.findOne({
                'email': {
                    $eq: model.email
                }
            })
            if (!user) {

                // create user
                user = await new db.user(model).save()
                console.log(user)

                // call sendOtp method
                await sendOtp(model, user, context)
                user.save();

            } else if (user && user.isVerified == false) {

                if (model.name) {
                    user.name = model.name
                }
                user.password = model.password

                // call send otp function
                await sendOtp(model, user, context)
                user.save();

            } else {
                log.end()
                throw new Error('User already exist')
            }
            log.end()
            return user
        }

    } catch (err) {
        log.end()
        throw new Error(err)
    }

}

const verifyUser = async (model, context) => {
    const log = context.logger.start('services/users/verifyUser')

    try {
        // find user
        const user = await db.user.findOne({
            'email': {
                $eq: model.email
            }
        })
        if (!user) {
            log.end()
            throw new Error('User not found')
        }

        // call matchOtp method
        await matchOtp(model, user, context)

        if (user) {

            // generate token
            const token = auth.getToken(user.id, false, context)
            if (!token) {
                throw new Error("token error")
            }
            user.token = token
            user.isVerified = true

            user.save()

        }

        log.end()
        return user;
    } catch (err) {
        log.end()
        throw new Error(err)
    }

}


const getById = async (id, context) => {
    const log = context.logger.start(`services/users/getById:${id}`)

    try {
        const user = id === 'my' ? context.user : await db.user.findById(id)
        log.end()
        return user

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const get = async (context) => {
    const log = context.logger.start(`api/users/get`)

    try {
        let data = []
        let users

        users = await db.user.find({}).sort({
            timeStamp: -1
        })
        // if user exist
        if (users) {
            for (const item of users) {
                data.push({
                    _id: item.id,
                    type: item.type,
                    isVerified: item.isVerified,
                    email: item.email,
                    name:item.name,
                    chapters: item.chapters,
                    profile: item.profile
                })
            }
            users = data
        }

        log.end()
        return users

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const update = async (id, model, context) => {
    const log = context.logger.start(`services/users:${id}`)
    try {

        const entity = id === 'my' ? context.user : await db.user.findById(id)

        if (!entity) {
            throw new Error('invalid user')
        }

        // call set method to update user
        await set(model, entity, context)

        log.end()
        return entity.save()
    } catch (err) {
        throw new Error(err)
    }
}

const login = async (model, context) => {
    const log = context.logger.start(`services/users/login`)

    try {

        let user;
        const query = {}

        if (model.email) {
            query.email = model.email
        }

        // find user
        user = await db.user.findOne(query)

        if (!user) { // user not found
            log.end()
            throw new Error('User not found')

        } else if (user && user.isVerified == false) { // user found but not verified

            // encrypt password
            user.password = encrypt.getHash(model.password, context)

            // call send otp function
            sendOtp(model, user, context)

            log.end()
            user.save();

        } else {

            // match password
            const isMatched = encrypt.compareHash(model.password, user.password, context)
            if (!isMatched) {
                log.end()
                throw new Error('Password mismatch')
            }

            // //  create token
            const token = auth.getToken(user._id, false, context)
            if (!token) {

                throw new Error("token error")
            }
            user.token = token
            user.save()

        }
        log.end()
        return user;

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}


const changePassword = async (model, context) => {
    const log = context.logger.start(`services/users/changePassword`)

    try {

        // find user
        const entity = await db.user.findOne({
            '_id': {
                $eq: context.user.id
            }
        })
        if (!entity) {
            throw new Error('invalid user')
        }

        // match old password
        const isMatched = encrypt.compareHash(model.password, entity.password, context)
        if (!isMatched) {
            throw new Error('Old password did not match.')
        }


        // update & encrypt password
        entity.password = encrypt.getHash(model.newPassword, context)

        log.end()
        return entity.save()

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}



const forgotPassword = async (model, context) => {
    const log = context.logger.start(`services/users/forgotPassword`)

    try {
        const query = {}
        if (model.email) {
            query.email = model.email
        }

        // find user
        const user = await db.user.findOne({
            'email': {
                $eq: query.email
            }
        })
        if (!user) {
            throw new Error('user not found')
        }

        // call send otp function
        await sendOtp(model, user, context)
        user.save()

        log.end()
        return user;

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const resetPassword = async (model, context) => {
    const log = context.logger.start(`services/users/verifyOtp`)

    try {

        let user;

        // find user
        user = await db.user.findOne({
            'email': {
                $eq: model.email
            }
        })
        if (!user) {
            throw new Error('user not found')
        }

        // call matchOtp method
        await matchOtp(model, user, context)

        // update password
        if (user) {
            user.password = encrypt.getHash(model.newPassword, context)
            user.save()
        }

        // confirmation mail 
        var transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: 'vermameenu0001@gmail.com',
                pass: 'Mn@01660'
            }
        });

        const subject = "Confirmation Mail"

        const temp = "This is confirmation that the password for your account" + " " + user.email + " " + "has just been changed.\n"

        await sendMail(user.email, transporter, subject, '', temp);

        user.tempToken = ''
        log.end()
        return user;

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}


const logOut = async (id, res, context) => {
    const log = context.logger.start(`services/users/logOut`)

    try {

        const user = await db.user.findOne({
            '_id': {
                $eq: context.user.id
            }
        })
        if (!user) {
            throw new Error('User not found')
        }
        user.token = ''
        user.save()
        res.message = 'logout successfully'
        log.end()
        return response.data(res, '')


    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

exports.verifyUser = verifyUser
exports.create = create
exports.getById = getById
exports.get = get
exports.update = update
exports.login = login
exports.forgotPassword = forgotPassword
exports.resetPassword = resetPassword
exports.changePassword = changePassword
exports.logOut = logOut














// exports.sendSms = sendSms
// exports.verifySms = verifySms




// const smsOtp = async (url, apikey, sender, subject, otp, phone) => {
//     let data;
//     data = 'apikey=' + apikey + '&sender=' + sender + '&numbers=' + phone + '&message=' + subject + otp;
//     // const data = JSON.stringify({})
//     https.post(`https://api.textlocal.in/send?${data}`, {}).then(response => {
//             console.log(response);
//         })
//         .catch(error => {
//             console.log(error);
//         });
// }

// const sendSms = async (req, model, context) => {
//     try {
//         const params = req.query;
//         let user;
//         if (params && (params.phone != undefined && params.phone != null)) {
//             user = await db.user.findOne({
//                 'phone': {
//                     $eq: params.phone
//                 }
//             })
//         }
//         if (user == undefined) {
//             throw new Error('User not found');
//         }
//         console.log("user", user)
//         // send message
//         if (user) {
//             // generate otp 
//             const otp = randomize('0', 6)
//             user.otp = otp;
//             // user.save();
//             const subject = "The verification code for Farmer's Hut is: "
//             // const text = "you are reseiving this because you have requested the reset of the password for your account"
//             smsOtp(url, apikey, sender, subject, otp, params.phone)
//         }

//         const date = new Date();
//         const expiryTime = moment(date.setMinutes(date.getMinutes() + 3));
//         user.expiryTime = expiryTime
//         // user.save();

//         const tempToken = auth.getToken(user._id, false, context)
//         if (!tempToken) {
//             throw new Error("token error")
//         }
//         user.tempToken = tempToken

//         return user.save();
//     } catch (err) {
//         console.log('unsuccessful');
//         throw new Error(err)
//     }
// }

// const verifySms = async (model, context) => {
//     try {
//         const query = {}
//         if (!model.tempToken) {
//             throw new Error("temp token required")
//         }
//         query.tempToken = model.tempToken
//         const user = await db.user.findOne({
//             'tempToken': {
//                 $eq: query.tempToken
//             }
//         })
//         if (!user) {
//             throw new Error('user not found')
//         }

//         const a = moment(new Date()).format();
//         const mom = moment(user.expiryTime).subtract(3, 'minutes').format();
//         const isExpired = moment(a).isBetween(mom, user.expiryTime)
//         if (!isExpired) {
//             throw new Error('otp expired')
//         }


//         // Check Otp entred
//         if (model.otp != user.otp) {
//             throw new Error("otp do not match")
//         }

//         // update password
//         user.password = encrypt.getHash(model.newPassword, context)
//         // user.otp = '',
//         // user.tempToken = ''
//         user.save();
//         user.otp = '',
//             user.tempToken = ''

//         return user;
//     } catch (err) {
//         console.log('unsuccessful');
//         throw new Error(err)
//     }
// }